public class ScoreBoard {
    private Team teamA;
    private Team teamB;

    public ScoreBoard(Team teamA, Team teamB) {
        this.teamA = teamA;
        this.teamB = teamB;
    }

    public String reactToGoal(String scoringTeam) {
        if (scoringTeam == this.teamA.getName()) {
            this.teamA.setScore(1);
            return String.valueOf(this.teamA.getScore());
        } else {
            this.teamB.setScore(1);
            return String.valueOf(this.teamB.getScore());

        }
    }

    public Team getScoredTeam(String scoringTeam){
        return scoringTeam == this.teamA.getName() ?  teamA : teamB;
    }
}
