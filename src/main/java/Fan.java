public class Fan {
    private Team team;

    public Fan(Team team) {
        this.team = team;
    }

    public String reactToGoal(String scoringTeam) {
        return (scoringTeam == this.team.getName()) ? "Ya" : "Boo";
    }

}
