public class Team {

    String name;
    Integer score;

    public Team(String name) {
        this.name = name;
        this.score = 0;
    }


    public String getName() {
        return name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score =+ score;
    }
}
