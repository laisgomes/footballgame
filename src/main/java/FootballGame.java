import java.util.List;

public class FootballGame {
    private Reporter reporter;
    private List<Fan> fans;
    private ScoreBoard scoreBoard;

    public FootballGame(Reporter reporter, List<Fan> fans, ScoreBoard scoreBoard) {
        this.reporter = reporter;
        this.fans = fans;
        this.scoreBoard = scoreBoard;

    }

    public void teamScored(String scoringTeam) {
        Team scoredTeam = scoreBoard.getScoredTeam(scoringTeam);
        reporter.reactToGoal(scoredTeam.getName());
        scoreBoard.reactToGoal(scoredTeam.getName());
        notifyFans(scoredTeam);


    }

    private void notifyFans(Team team){
        fans.forEach(fan -> fan.reactToGoal(team.getName()));
    }
}
