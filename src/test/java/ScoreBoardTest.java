import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ScoreBoardTest {

    @Test
    public void shouldChangeReactTeamScored() {
        Team teamA = new Team("Discovery");
        Team teamB = new Team("The Defiant");
        ScoreBoard scoreBoard = new ScoreBoard(teamA, teamB);

        assertEquals("1", scoreBoard.reactToGoal(teamA.getName()));
        assertEquals("1", scoreBoard.reactToGoal(teamB.getName()));

    }
}