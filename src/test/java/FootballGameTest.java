import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FootballGameTest {

    private FootballGame footballGame;
    private ScoreBoard scoreBoard;
    private  Team teamA;
    private  Team teamB;
    private Reporter reporter;
    private Fan fan1;
    private Fan fan2;

    @Before
    public void setUp() {
        reporter = mock(Reporter.class);
        teamA = new Team("Team A");
        teamB = new Team("Team B");
        scoreBoard = mock(ScoreBoard.class);
        fan1 = mock(Fan.class);
        fan2 = mock(Fan.class);

        List<Fan> fans = Arrays.asList(fan1, fan2);

        footballGame = new FootballGame(reporter, fans, scoreBoard);
    }

    @Test
    public void returnsReactForTeamAGoal() {
        when(scoreBoard.getScoredTeam("Team A")).thenReturn(teamA);
        footballGame.teamScored("Team A");
        verify(reporter).reactToGoal(teamA.getName());
        verify(scoreBoard).reactToGoal(teamA.getName());
        verify(fan1).reactToGoal(teamA.getName());
        verify(fan2).reactToGoal(teamA.getName());
    }
}