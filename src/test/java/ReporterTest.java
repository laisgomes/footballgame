import org.junit.Test;

import static org.junit.Assert.*;

public class ReporterTest {

    @Test
    public void returnReporterReactToGoal() {
        Reporter reporter = new Reporter();
        assertEquals("GOAL by TeamA", reporter.reactToGoal("TeamA"));
    }
}