import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FanTest {

    private Fan fan;

    @Before
    public void setUp() {
        Team teamA = new Team("Team A");
        fan = new Fan(teamA);
    }
    @Test
    public void returnYaWhenTeamA() {
        assertEquals("Ya", fan.reactToGoal("Team A"));
    }

    @Test
    public void returnBooWhenTeamB() {
        assertEquals("Boo", fan.reactToGoal("Team B"));
    }
}